package com.tselawe.genie9.players.viewmodel;

import android.app.Activity;

import androidx.lifecycle.MutableLiveData;

import com.tselawe.genie9.R;
import com.tselawe.genie9.playerdetail.view.PlayerDetailsActivity;
import com.tselawe.genie9.players.model.PlayerModel;

public class PlayerItemViewModel {

    public MutableLiveData<Integer> backgroundColor = new MutableLiveData<>();
    public MutableLiveData<Activity> activity = new MutableLiveData<>();

    public PlayerItemViewModel(PlayerModel playerModel) {
        this.playerModel = playerModel;
    }

    private PlayerModel playerModel;

    /**
     * Method name: Set player background color
     * Action: Change the color of the background based on specific information came from API
     * @Param {position}
      */
    public void setPlayerBackground() {
        switch (playerModel.getPosition()) {
            case "G-F":
                backgroundColor.postValue(R.color.green);
                break;
            case "C":
                backgroundColor.postValue(R.color.orange);
                break;
            case "F":
                backgroundColor.postValue(R.color.purple);
                break;
            case "G":
                backgroundColor.postValue(R.color.red);
                break;
            default:
                backgroundColor.postValue(R.color.blue);
        }
    }

    public void onPlayerClick() {
        activity.postValue(new PlayerDetailsActivity());
    }
}
