package com.tselawe.genie9.players.remote;

import com.tselawe.genie9.players.model.PlayerListModel;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface name: All endpoints and method will be mentioned here
 * @Method type {GET, POST, PUT, DELETE}
 * @Endpoint Url
 * @QueryParam
 */
public interface IPlayersApi {
    @GET("players")
    Observable<Response<PlayerListModel>> getPlayerList(@Query("page") String page, @Query("per_page") String pre_Page);

}
