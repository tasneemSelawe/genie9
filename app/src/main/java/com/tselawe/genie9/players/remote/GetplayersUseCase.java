package com.tselawe.genie9.players.remote;

import com.tselawe.genie9.network.RequestHelper;
import com.tselawe.genie9.players.model.PlayerListModel;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class GetplayersUseCase {
    public Map<String, String> params;


    public Observable<Response<PlayerListModel>> get() {
        return requestServer().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Use case name: get players list
     * @return ArrayList<PlayerModel>
     */
    private Observable<Response<PlayerListModel>> requestServer() {
        return RequestHelper.getInstance().create(IPlayersApi.class).
                getPlayerList(params.get("page"), params.get("per_page"));
    }

    /**
     * Name: Set query queryParam in api endpoint
     * @param queryParam
     * @return
     */
    public GetplayersUseCase setParams(Map<String, String > queryParam) {
        this.params = queryParam;
        return this;
    }
}
