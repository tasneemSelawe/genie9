package com.tselawe.genie9.players.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class PlayerListModel {


    @SerializedName("data")
    @Expose
    private List<PlayerModel> data = new ArrayList();
    @SerializedName("meta") @Expose
    private MetaModel meta;

    public List<PlayerModel> getData() {
        return data;
    }

    public void setData(List<PlayerModel> data) {
        this.data = data;
    }

    public MetaModel getMeta() {
        return meta;
    }

    public void setMeta(MetaModel meta) {
        this.meta = meta;
    }
}