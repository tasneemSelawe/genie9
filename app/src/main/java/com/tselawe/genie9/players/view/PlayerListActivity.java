package com.tselawe.genie9.players.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.tselawe.genie9.R;
import com.tselawe.genie9.players.adapter.PlayersAdapter;
import com.tselawe.genie9.players.model.PlayerModel;
import com.tselawe.genie9.players.viewmodel.PlayersViewModel;

import java.util.List;

public class PlayerListActivity extends AppCompatActivity {

    private PlayersViewModel playersViewModel;
    private PlayersAdapter playersAdapter;
    private RecyclerView rvPlayers;
    private boolean loading = false;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_list);
        rvPlayers = findViewById(R.id.rv_players);
        playersViewModel = new PlayersViewModel();
        playersAdapter = new PlayersAdapter(playersViewModel);
        playersAdapter.setHasStableIds(true);
        rvPlayers.setAdapter(playersAdapter);
        playersViewModel.getPlayersData(currentPage, 20);
        observePlayerList();
        recyclerViewPagination();
        observeLoading();

    }

    private void observeLoading() {
        playersViewModel.loading.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer loading) {
            }
        });
    }

    private void recyclerViewPagination() {
        rvPlayers.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = layoutManager.getItemCount();
                Log.d("total",totalItemCount+"");
                int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                Log.d("total_",lastVisibleItem+"");
                if (lastVisibleItem == totalItemCount-1) {
                    playersViewModel.getPlayersData(currentPage++, 20);
                }
            }
        });
    }

    private void observePlayerList() {
        playersViewModel.players.observe(this, new Observer<List<PlayerModel>>() {
            @Override
            public void onChanged(List<PlayerModel> playerModels) {
                playersAdapter.setPlayerList(playerModels);
            }
        });
    }
}
