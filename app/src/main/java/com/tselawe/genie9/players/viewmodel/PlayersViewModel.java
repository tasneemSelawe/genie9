package com.tselawe.genie9.players.viewmodel;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.tselawe.genie9.playerdetail.view.PlayerDetailsActivity;
import com.tselawe.genie9.players.model.PlayerListModel;
import com.tselawe.genie9.players.model.PlayerModel;
import com.tselawe.genie9.players.remote.GetplayersUseCase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.functions.Consumer;
import retrofit2.Response;

public class PlayersViewModel {
    public MutableLiveData<List<PlayerModel>> players = new MutableLiveData();
    private List<PlayerModel> playerModelList =new ArrayList();
    public MutableLiveData<Integer> loading=new MutableLiveData<>();

    public void getPlayersData(int page, int numPerPage) {
        GetplayersUseCase getplayersUseCase = new GetplayersUseCase();
        Map<String, String> params = new HashMap();
        params.put("page", page + "");
        params.put("per_page", numPerPage + "");

        /**
         * Name: Response handler
         * Action: Handle the response status codes
         */
        getplayersUseCase.setParams(params).get().subscribe(new Consumer<Response<PlayerListModel>>() {
            @Override
            public void accept(Response<PlayerListModel> playerListModelResponse) throws Exception {
                Log.d("code", playerListModelResponse.code() + "");
                switch (playerListModelResponse.code()) {
                    case 200:
                        playerModelList.addAll(playerListModelResponse.body().getData());
                        players.postValue(playerModelList);
                        break;
                    case 400:
                        break;
                    case 404:
                        break;
                    case 500:
                        break;
                }

            }

        }, throwable -> {
            Log.d("error", throwable.getMessage());
        });
    }



}
