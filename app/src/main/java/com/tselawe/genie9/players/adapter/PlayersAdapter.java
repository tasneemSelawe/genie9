package com.tselawe.genie9.players.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tselawe.genie9.R;
import com.tselawe.genie9.databinding.ActivityPlayerDetailsBinding;
import com.tselawe.genie9.databinding.RowPlayerBinding;
import com.tselawe.genie9.databinding.RowProgressbarBinding;
import com.tselawe.genie9.playerdetail.view.PlayerDetailsActivity;
import com.tselawe.genie9.players.model.PlayerModel;
import com.tselawe.genie9.players.viewmodel.PlayerItemViewModel;
import com.tselawe.genie9.players.viewmodel.PlayersViewModel;

import java.util.ArrayList;
import java.util.List;

public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.BaseViewHolder> {


    private List<PlayerModel> playerList = new ArrayList();
    private PlayersViewModel playersViewModel;
    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private int loading;
    private RowPlayerBinding bindingPlayer;
    private RowProgressbarBinding bindingLoading;


    /**
      Constructor of Adapter class
     *
* @param playersViewModel
     */public PlayersAdapter(PlayersViewModel playersViewModel) {
        this.playersViewModel = playersViewModel;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {

            case VIEW_TYPE_NORMAL:
                bindingPlayer = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                        , R.layout.row_player, parent, false);
                return new DataViewHolder(bindingPlayer.getRoot());
            case VIEW_TYPE_LOADING:
                bindingLoading = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                        , R.layout.row_progressbar, parent, false);
                return new ProgressbarViewHolder(bindingLoading.getRoot());
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case VIEW_TYPE_NORMAL:
                DataViewHolder dataHolder = (DataViewHolder) holder;
                dataHolder.setData(playerList.get(position));
                dataHolder.bindLayout(bindingPlayer);
                break;
            case VIEW_TYPE_LOADING:
                ProgressbarViewHolder progressHolder = (ProgressbarViewHolder) holder;
                progressHolder.bindLayout(bindingLoading);
                break;
        }
    }

    /**
     * Name: Observer method
     * Action: Navigate to player details page
    *
* @param dataViewHolder
* @param playerModel
* @param playerItemViewModel
     */private void observeNextPage(DataViewHolder dataViewHolder,PlayerModel playerModel,PlayerItemViewModel playerItemViewModel) {
        playerItemViewModel.activity.observe((LifecycleOwner) dataViewHolder.itemView.getContext(), new Observer<Activity>() {
            @Override
            public void onChanged(Activity playerDetailsActivity) {
                Intent intent = new Intent(dataViewHolder.itemView.getContext(), playerDetailsActivity.getClass());
                intent.putExtra("PLAYER", (Parcelable) playerModel);
                dataViewHolder.itemView.getContext().startActivity(intent);
            }
        });
    }

    /**
     * Name: Set player List
     * Action: Fill the players in list from the requested API.
     *
    *
* @param playerList
     */public void setPlayerList(List<PlayerModel> playerList) {
        this.playerList=playerList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return playerList == null ? 0 : playerList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        return position == playerList.size()  ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
    }

    /**
    * Base view holder
     * Actoin: to be extended to make binding

* @param <T>
     */abstract class BaseViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
        public BaseViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        abstract void bindLayout(T item);
    }

    class ProgressbarViewHolder extends BaseViewHolder<RowProgressbarBinding> {
        public ProgressbarViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        void bindLayout(RowProgressbarBinding item) {
           item.progressBar.setVisibility(loading);
        }

    }

    class DataViewHolder extends BaseViewHolder<RowPlayerBinding> {
        PlayerModel playerModel;

        public DataViewHolder(@NonNull View itemView) {
            super(itemView);

        }

        void setData(PlayerModel playerModel) {
            this.playerModel = playerModel;
        }

        @Override
        void bindLayout(RowPlayerBinding itemBinding) {
            itemBinding.setPlayerModel(playerModel);
            PlayerItemViewModel playerItemViewModel = new PlayerItemViewModel(playerModel);
            itemBinding.setPlayerItemVM(playerItemViewModel);
            int playerId = playerModel.getId();
            String imageUrl = "https://cdn2.thecatapi.com/images/{id}.jpg";
            String newImageUrl = imageUrl.replace("{id}", playerId + "");

            Glide.with(itemBinding.getRoot().getContext())
                    .load(newImageUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .into(itemBinding.ivPlayer);
            observeBackgroundColor(playerItemViewModel,itemBinding);
            observeNextPage(this,playerModel,playerItemViewModel);
            playerItemViewModel.setPlayerBackground();

        }

        private void observeBackgroundColor(PlayerItemViewModel playerItemViewModel,RowPlayerBinding itemBinding) {
            playerItemViewModel.backgroundColor.observe((LifecycleOwner) itemView.getContext(), new Observer<Integer>() {
                @Override
                public void onChanged(Integer color) {
                    itemBinding.clPlayerInfo.setBackground(itemBinding.getRoot().getContext().getResources().getDrawable(color));

                }
            });

        }

    }

    @Override
    public long getItemId(int position) {
        if(position>playerList.size())
        return playerList.get(position).getId();
        else
            return 0;
    }
}



