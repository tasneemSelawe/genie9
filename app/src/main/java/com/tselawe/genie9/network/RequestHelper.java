package com.tselawe.genie9.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.lang.annotation.Documented;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*

 */
public class RequestHelper {
    private static RequestHelper instance;
    private Retrofit retrofit;

    private RequestHelper() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://balldontlie.io/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getClient())
                .build();
    }


    public static RequestHelper getInstance() {
        synchronized (RequestHelper.class) {
            if (instance == null) {
                instance = new RequestHelper();
            }
        }
        return instance;
    }

    public <T> T create(Class<T> clazz) {
        return retrofit.create(clazz);
    }


    private OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder builder = chain.request().newBuilder();
                        return chain.proceed(builder.build());
                    }
                }).build();
    }

}
