package com.tselawe.genie9.playerdetail.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
import com.tselawe.genie9.R;
import com.tselawe.genie9.databinding.ActivityPlayerDetailsBinding;
import com.tselawe.genie9.players.model.PlayerModel;

public class PlayerDetailsActivity extends AppCompatActivity {

    private PlayerModel playerModel;
    private ActivityPlayerDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_player_details);
        playerModel = getIntent().getParcelableExtra("PLAYER");
        String imageUrl = "https://cdn2.thecatapi.com/images/{id}.jpg";
        String newImageUrl = imageUrl.replace("{id}", playerModel.getId() + "");
        Log.d("player_id",newImageUrl);
        Glide.with(this).load(newImageUrl).into(binding.ivPlayer);
        binding.setPlayerModel(playerModel);
    }
}
